ftags
=====

A tool to tag files using extended attributes. 

Description:
------------
`ftags` can:

 - set a tag on a file (`-t` can be specified multiple times):
```
$ ftags -t tag_value my_file
```
 - list the tags of a file:
```
$ ftags -l my_file
```
 - clear the tags of a file:
```
$ ftags -c my_file
```
 - search for files associated to a tag
```
$ ftags -s some_tag
```

Tags can contain any UTF-8 character, except the separator (see Customization
below) which defaults to the colon character (`:`).


Installation:
-------------
```
$ cargo install --git https://framagit.org/rogarb/ftags
````

Customization:
--------------
This works by setting an extended attribute key in the `user` namespace, which 
contains a list of tags. Both the key and the separator can be customized by 
changing their value in the file `src/custom_params.rs`.
