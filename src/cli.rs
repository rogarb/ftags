//! This module defines the command line interface of the program
use anyhow::{bail, Result};
use clap::{arg, command, value_parser, ArgGroup};

use crate::commands;
use crate::tag::Tag;

use std::path::PathBuf;

pub struct Cli {}

impl Cli {
    pub fn run() -> Result<()> {
        let args = command!()
            .about("A tool to tag files using extended attributes.")
            .after_help("TARGET must be a folder when using the --search command, in that case it is not required (defaults to $HOME). \nIn the other clapses, it can be a folder, a file or a symlink and it is required.")
            .arg(arg!([TARGET] "File or folder").value_parser(value_parser!(PathBuf)))
            .group(ArgGroup::new("command").required(true).multiple(false))
            .arg(
                arg!(-t --tag <TAG> "Set TAG on TARGET")
                    .requires("TARGET")
                    .group("command")
                    .value_parser(value_parser!(Tag))
            )
            .arg(
                arg!(-l --list "List the tags associated to TARGET")
                    .requires("TARGET")
                    .group("command"),
            )
            .arg(
                arg!(-c --clear "Clear the tags associated to TARGET")
                    .requires("TARGET")
                    .group("command"),
            )
            .arg(
                arg!(-s --search <TAG> "Search TAG in file contained in TARGET")
                    .group("command")
                    .value_parser(value_parser!(Tag))
                )
            .get_matches();

        if args.get_flag("list") {
            // --list requires target, ensured by clap
            commands::list_tags(
                args.get_one::<std::path::PathBuf>("TARGET")
                    .unwrap()
                    .as_path(),
            )?;
        } else if args.get_flag("clear") {
            // --clear requires target, ensured by clap
            commands::clear_tags(
                args.get_one::<std::path::PathBuf>("TARGET")
                    .unwrap()
                    .as_path(),
            )?;
        } else if let Some(tag) = args.get_one::<Tag>("tag") {
            commands::set_tag(
                tag,
                args.get_one::<std::path::PathBuf>("TARGET")
                    .unwrap()
                    .as_path(),
            )?;
        } else if let Some(tag) = args.get_one::<Tag>("search") {
            commands::search_tag(
                tag,
                args.get_one::<std::path::PathBuf>("TARGET")
                    .unwrap_or(&dirs::home_dir().unwrap_or_else(|| PathBuf::from("/")))
                    .as_path(),
            )?;
        } else {
            bail!("BUG: no command found!")
        }

        Ok(())
    }
}
