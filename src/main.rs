use anyhow::Result;

mod cli;
mod commands;
mod custom_params;
mod tag;

fn main() -> Result<()> {
    cli::Cli::run()
}
