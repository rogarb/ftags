//! This module can be edited to customize the key and separator settings.
//!

/// The key used for storing the tags. It must belong to the `user` namespace,
/// i.e. the "user." part must remain untouched.
pub const KEY: &str = "user.tags";

/// The separator character to use. It cannot be used in a tag.
pub const SEP: char = ':';
