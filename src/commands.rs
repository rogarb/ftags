//! This module contains the implementation of the xattr commands.
use anyhow::{bail, Result};
use walkdir::WalkDir;

use std::path::Path;

use crate::tag::{Tag, TagList, KEY};

pub fn set_tag(tag: &Tag, path: &Path) -> Result<()> {
    let mut tag = tag.to_owned();
    tag.append(xattr::get(path, KEY)?)?;
    xattr::set(path, KEY, tag.as_bytes())?;
    Ok(())
}
pub fn clear_tags(path: &Path) -> Result<()> {
    match xattr::get(path, KEY)? {
        Some(_) => {
            println!("Clearing tags for file {}", path.display());
            xattr::remove(path, KEY)?;
        }
        None => bail!("No tags for file {}", path.display()),
    }
    Ok(())
}
pub fn list_tags(path: &Path) -> Result<()> {
    match xattr::get(path, KEY)? {
        Some(value) => println!("{}:\n{}", path.display(), TagList::try_from(value)?),
        None => println!("File {} has no tag", path.display()),
    }
    Ok(())
}
pub fn search_tag(tag: &Tag, folder: &Path) -> Result<()> {
    println!("Searching for tag '{tag}' in {}", folder.display());
    let mut found = false;
    for entry in WalkDir::new(folder) {
        match entry {
            Ok(entry) => match xattr::get(entry.path(), KEY) {
                Ok(res) => {
                    if let Some(tags) = res.map(TagList::try_from) {
                        if tags?.has_tag(tag) {
                            found = true;
                            println!("{tag}: {}", entry.path().display());
                        }
                    }
                }
                Err(e) => println!("{}: {e}", entry.path().display()),
            },
            Err(e) => println!("{e}"),
        }
    }
    if !found {
        println!("No file found with tag '{tag}'");
    }
    Ok(())
}
