//! This module defines the Tag object, which is the representation of a tag.
//!
//! This implementation accepts any character in the tag, except the separator.
//!
use anyhow::anyhow;

use std::ffi::{OsStr, OsString};
use std::os::unix::ffi::OsStringExt;

pub use crate::custom_params::KEY;
use crate::custom_params::SEP;

#[derive(Clone, PartialEq, Eq)]
pub struct Tag(String);

impl From<String> for Tag {
    fn from(s: String) -> Self {
        Self(s)
    }
}

// impl TryFrom<OsString> for Tag {
//     type Error = std::string::FromUtf8Error;
//     fn try_from(os_string: OsString) -> Result<Self, Self::Error> {
//         String::from_utf8(os_string.into_vec()).map(|v| {
//             if v.find(':').is_some() {
//                 Self(v.replace(':', "\\:"))
//             } else {
//                 Self(v)
//             }
//         })
//     }
// }
//
// impl TryFrom<&OsStr> for Tag {
//     type Error = std::string::FromUtf8Error;
//     fn try_from(os_str: &OsStr) -> Result<Self, Self::Error> {
//         Self::try_from(os_str.to_os_string())
//     }
// }

impl TryFrom<OsString> for Tag {
    type Error = anyhow::Error;
    fn try_from(os_string: OsString) -> Result<Self, Self::Error> {
        match String::from_utf8(os_string.into_vec()) {
            Ok(s) => {
                // A tag shouldn't contain a colon
                if s.find(SEP).is_some() {
                    Err(anyhow!("A tag shouldn't contain the '{SEP}' character"))
                } else {
                    Ok(Self(s))
                }
            }
            Err(e) => Err(anyhow!("{e}")),
        }
    }
}

impl TryFrom<&OsStr> for Tag {
    type Error = anyhow::Error;
    fn try_from(os_str: &OsStr) -> Result<Self, Self::Error> {
        Self::try_from(os_str.to_os_string())
    }
}

impl Tag {
    pub fn append(&mut self, existing_tag: Option<Vec<u8>>) -> anyhow::Result<()> {
        if let Some(value) = existing_tag {
            self.0.push(SEP);
            self.0.push_str(std::str::from_utf8(&value)?);
        }
        Ok(())
    }

    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }
}

impl std::fmt::Display for Tag {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.0)
    }
}
pub struct TagList(Vec<Tag>);

impl std::fmt::Display for TagList {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        let string: String = self.0.iter().map(|s| format!(" - {}\n", s)).collect();
        write!(f, "{}", string)
    }
}

impl TryFrom<Vec<u8>> for TagList {
    type Error = std::string::FromUtf8Error;
    fn try_from(data: Vec<u8>) -> Result<Self, Self::Error> {
        String::from_utf8(data).map(|res| Self(res.split(SEP).map(|s| Tag(s.to_owned())).collect()))
    }
}

impl TagList {
    pub fn has_tag(&self, tag: &Tag) -> bool {
        self.0.iter().any(|t| t == tag)
    }
}
